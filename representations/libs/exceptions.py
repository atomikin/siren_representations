__author__ = 'atomikin'


class SirenException(Exception):
    pass


class SirenActionException(SirenException):
    pass


class SirenDumpException(SirenException):
    pass



