from representations.bin.siren_object_api_impl import SirenObjectAPI
from representations.bin.types.json import JSON

__author__ = 'atomikin'


class SirenObject(SirenObjectAPI):
    IMPLEMENTATION = JSON
