from representations.bin.entity import Entity
from representations.bin.siren_object import SirenObject
from representations.libs.utils.body import Body


class Representation(SirenObject):

    LIMITED = ('links', 'properties', 'actions', 'entities', 'class')
    REQUIRED = ('class', )

    def __init__(self, *args, model=None, **kwargs):
        self.model = model
        self._body = self._body = Body(types_description={'list': ['links', 'entities', 'class', 'actions']})
        self.body.append({'class': self.obj_name.lstrip('Dy_')})

    def dump(self, indent=None) -> str:
        return super().dump(indent=indent)

    # @classmethod
    # def load(cls, text):
    #     loaded = super().load(text)
    #     res = cls()
    #     res.body.append(loaded)

    @classmethod
    def create(cls, *args, **kwargs):
        if isinstance(args[0], (list, tuple)):
            collection_csl = type('{}Collection'.format(cls.__name__), (RepresentationCollection, cls), {})
            return collection_csl.create(*args, **kwargs)
        return super().create(*args, **kwargs)


class EmbeddedRepresentation(Representation, Entity):
    """
    Class representing an embedded resources
    """

    def __init__(self, model):
        """
        :param resource: has to be a descendant of AbstractSerializable
        :return: self
        """
        super().__init__(model)
        self._class = 'entities'


class RepresentationCollection(Representation):

    def __init__(self, collection, *args, **kwargs):
        super().__init__(collection, *args, **kwargs)
        self.model = None
        self.models = collection
        t = type(self)
        t_name = t.__name__.lstrip('Dy_').replace('Collection', '')
        cls = [i for i in reversed(t.__mro__) if (i.__name__ == t_name)][0]
        # cls = type(cls.__name__, (EmbeddedRepresentation, cls,), dict())
        for r in collection:
            print(r)
            self.append({'entities': cls.create(r, *args, **kwargs)})

    @classmethod
    def create(cls, *args, mixins=tuple(), **kwargs):
        if cls.IMPLEMENTATION and cls.IMPLEMENTATION not in mixins:
            mixins = tuple(mixins) + (cls.IMPLEMENTATION,)
        if mixins:
            Type = type('Dy_{}'.format(cls.__name__), (cls, ) + tuple(mixins), dict())
            obj = Type(*args, **kwargs)
            return obj
        else:
            return cls(*args, **kwargs)








