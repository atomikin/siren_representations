from setuptools import setup, find_packages

setup(
    name='representations',
    version='1.0.3',
    author='atomikin',
    description='A tool for building of siren representations.',
    platforms='any',
    packages=find_packages()
)